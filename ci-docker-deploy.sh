#!/usr/bin/env bash

if [ -n "$1" ]
then
    newVersion=$1
    
    if [ -n "$2" ]
    then
        dockerComposeFile=$2
        
        if [ -n "$3" ]
        then
            envFile=$3
            
            if [ -n "$4" ]
            then
                dockerfileName=$4
                
                if [ -n "$5" ]
                then
                    isDownOld=$5
                else
                    isDownOld=1
                fi
            
                image=$(awk '/image:/ {print $2}' "$dockerComposeFile")
                repository=$(echo "$image" | awk -F ':' '{print $1}')
                oldVersion=$(sed '/^Version=/!d;s/.*=//' "$envFile")
                
                oldImageId=$(docker images -q "$repository":"$oldVersion")
                
                echo "执行 docker build -t $repository:$newVersion"
                docker build -f "$dockerfileName" -t "$repository":"$newVersion" .
                
                if [ "$isDownOld" == "1" ]
                then
                    echo "执行 docker compose down"
                    docker compose -f "$dockerComposeFile" down
                fi
                
                echo "修改 $envFile：Version=$newVersion"
                sed -i "1s/.*/Version=$newVersion/" "$envFile"
                
                if [ -n "$oldImageId" ]
                then
                    echo "存在镜像 $repository:$oldVersion - $oldImageId, 移除之：docker rmi -f $oldImageId"
                    docker rmi -f "$oldImageId"
                else
                    echo "不存在镜像 $repository:$oldVersion"
                fi
            fi
        fi
    fi
fi