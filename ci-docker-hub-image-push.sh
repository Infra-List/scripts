#!/usr/bin/env bash

if [ -n "$1" ]
then
    repository=$1
    
    if [ -n "$2" ]
    then
        newVersion=$2
        
        if [ -n "$3" ]
        then
            env=$3
            
            if [ -n "$DOCKER_HUB_URL" ]
            then
                if [ "$env" == "alpha" ]
                then
                    dockerHubNamespace="$DOCKER_HUB_NAMESPACE_TEST"
                else
                    dockerHubNamespace="$DOCKER_HUB_NAMESPACE"
                fi
                            
                echo "执行 docker login $DOCKER_HUB_URL"
                docker login "$DOCKER_HUB_URL" --username "$DOCKER_HUB_USER_NAME" --password "$DOCKER_HUB_PASSWORD"
                
                echo "执行 docker tag $repository:$newVersion $DOCKER_HUB_URL/$dockerHubNamespace/$repository:$newVersion"
                docker tag "$repository":"$newVersion" "$DOCKER_HUB_URL"/"$dockerHubNamespace"/"$repository":"$newVersion"
                
                echo "docker push $DOCKER_HUB_URL/$dockerHubNamespace/$repository:$newVersion"
                docker push "$DOCKER_HUB_URL"/"$dockerHubNamespace"/"$repository":"$newVersion"
            fi
        fi
    fi
fi