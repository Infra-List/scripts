#!/usr/bin/env bash

url="http://www.pushplus.plus/send"

if [ -n "$1" ]
then
    token=$1
    
    if [ -n "$2" ]
    then
        tag=$2
        
        if [ -n "$3" ]
        then
            message=$3
        
            curl -X 'POST' \
              ''"${url}"'' \
              -H 'Content-Type: application/json' \
              -d '{
              "template":"markdown",
              "token":"'"${token}"'", 
              "title":"'"${tag}"'", 
              "content":"'"${message}"'"
            }'
        fi
    fi
else
    echo "还没有设置 pushplus token"
fi