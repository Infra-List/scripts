#!/usr/bin/env bash

url="https://oapi.dingtalk.com/robot/send?access_token="

if [ -n "$1" ]
then
    accessToken=$1
    
    if [ -n "$2" ]
    then
        tag=$2
        
        if [ -n "$3" ]
        then
            message=$3
        
            curl -X 'POST' \
              ''"${url}"''"${accessToken}"'' \
              -H 'Content-Type: application/json' \
              -d '{
              "msgtype":"markdown",
              "markdown":{
                "title":"'"${tag}"'", 
                "text":"Tag：'"${tag}"' \n\n '"${message}"'"
              }
            }'
        fi
    fi
else
    echo "还没有设置 Dingtalk access token"
fi