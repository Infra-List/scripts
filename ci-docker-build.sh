#!/usr/bin/env bash

if [ -n "$1" ]
then
    repository=$1
    
    if [ -n "$2" ]
    then
        newVersion=$2
        
        if [ -n "$3" ]
        then
            dockerfileName=$3
            
            oldImageIds=$(docker images -q "$repository")
            for oldImageId in $oldImageIds;do
              echo "存在镜像 $repository - $oldImageId, 移除之：docker rmi -f $oldImageId"
              docker rmi -f "$oldImageId"
            done
            
            echo "执行 docker build -t $repository:$newVersion"
            docker build -f "$dockerfileName" -t "$repository":"$newVersion" .
            
#            oldImageCount=$(docker images -q "$repository" | wc -l)
#            if [[ $oldImageCount -gt 1 ]]; then
#              for oldImageId in $oldImageIds;do
#                echo "存在镜像 $repository - $oldImageId, 移除之：docker rmi -f $oldImageId"
#                docker rmi -f "$oldImageId"
#              done
#            fi
        fi
    fi
fi