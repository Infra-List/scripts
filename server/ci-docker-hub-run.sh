#!/usr/bin/env bash

dockerEnvFile=/home/docker/env
dockerFolder=/home/docker/docker

if [ -n "$1" ]
then
    repository=$1
    
    if [ -n "$2" ]
    then
        newVersion=$2
        
        if [ -n "$3" ]
        then
            envFile="$dockerFolder/$repository/$3"
            
            dockerHubUserName=$4
            dockerHubPassword=$5
            
            dockerHubUrl=$(sed '/^DockerHubUrl=/!d;s/.*=//' "$dockerEnvFile")
            dockerHubNamespace=$(sed '/^DockerHubNamespace=/!d;s/.*=//' "$dockerEnvFile")
            
            if [ -n "$dockerHubUserName" ] && [ -n "$dockerHubPassword" ];
            then
              echo "执行 docker login $dockerHubUrl"
              docker login "$dockerHubUrl" --username "$dockerHubUserName" --password "$dockerHubPassword"
            fi
            
            dockerComposeFileName="$dockerFolder/$repository/docker-compose.yml"
            echo "执行 docker compose down"
            docker compose -f "$dockerComposeFileName" down
            
            oldImageIds=$(docker images -q "$dockerHubUrl/$dockerHubNamespace/$repository")
            for oldImageId in $oldImageIds;do
              echo "存在镜像 $repository - $oldImageId, 移除之：docker rmi -f $oldImageId"
              docker rmi -f "$oldImageId"
            done
            
            echo "修改 $envFile：Version=$newVersion"
            sed -i "1s/.*/Version=$newVersion/" "$envFile"
                        
            echo "执行 docker compose -f $dockerComposeFileName up -d"
            docker compose -f "$dockerComposeFileName" --env-file "$envFile" --env-file "$dockerEnvFile" up -d
        fi
    fi
fi