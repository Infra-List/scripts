#!/usr/bin/env bash

hubUrl=192.168.100.211:25000
hubName=hy
hubPassword=AAss3388

if [ -n "$1" ]
then
    dockerComposeFileName=$1
    
    if [ -n "$2" ]
    then
        envFile=$2
        
        echo "执行 docker login $hubUrl --username $hubName"
        docker login $hubUrl --username $hubName --password $hubPassword
        
        image=$(awk '/image:/ {print $2}' "$dockerComposeFileName")
        repository=$(echo "$image" | awk -F ':' '{print $1}')
        version=$(sed '/^Version=/!d;s/.*=//' "$envFile")
        
        echo "执行 docker tag $repository:$version $hubUrl/$repository:$version"
        docker tag "$repository":"$version" $hubUrl/"$repository":"$version"
        
        echo "docker push $hubUrl/$repository:$version"
        docker push $hubUrl/"$repository":"$version"
    fi
fi