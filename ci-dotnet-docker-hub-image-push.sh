#!/usr/bin/env bash

scriptsFolder=scripts

if [ -n "$1" ]
then
    projectFolder=$1
    
    if [ -n "$2" ]
    then
        env=$2
        
        newVersion=$(awk '/<\/*AssemblyVersion\/*>/{gsub(/[[:space:]]*<\/*AssemblyVersion\/*>/,"");print $0}' "$projectFolder/$projectFolder.csproj")
        projectName=$(awk '/<\/*AssemblyTitle\/*>/{gsub(/[[:space:]]*<\/*AssemblyTitle\/*>/,"");print $0}' "$projectFolder/$projectFolder.csproj")
        
        if [ "$env" == "alpha" ]
        then
            newVersion="$newVersion-alpha"
        fi
        
        . $scriptsFolder/ci-docker-hub-image-push.sh "$projectName" "$newVersion" "$env"
    fi
fi