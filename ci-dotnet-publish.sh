#!/usr/bin/env bash

mkdir -p "./bin/Release/net8.0/zh-hant"
mkdir -p "./bin/Release/net8.0/zh-hans"
mkdir -p "./app/publish"

echo "执行 dotnet restore --interactive"
dotnet restore --interactive

echo "执行 dotnet build --configuration Release"
dotnet build --configuration Release

echo "执行 dotnet publish --configuration Release --output app/publish"
dotnet publish --configuration Release --output app/publish