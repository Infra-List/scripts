#!/usr/bin/env bash

dockerFolder=/home/docker/docker
scriptsFolder=scripts

if [ -n "$1" ]
then
    projectName=$1
    
    if [ -n "$2" ]
    then
        env=$2
    else
        env="test"
    fi
    
    version=$(sed -n 's/.*"version": *"\([^"]*\)".*/\1/p' package.json)
    
    if [ "$env" == "test" ]
    then
        newVersion=$version-test
        dockerfileName=Dockerfile-test
        envFile="$dockerFolder/$projectName/.env"
        isDownOld=1
    else
        newVersion=$version
        dockerfileName=Dockerfile
        envFile="$dockerFolder/$projectName/.env.production"
        isDownOld=0
    fi
    
    dockerComposeFileName="$dockerFolder/$projectName/docker-compose.yml"
    
    . $scriptsFolder/ci-docker-deploy.sh "$newVersion" "$dockerComposeFileName" "$envFile" "$dockerfileName" $isDownOld
fi