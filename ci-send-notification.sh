#!/usr/bin/env bash

scriptsFolder=scripts

if [ -n "$1" ]
then
    tag=$1
    
    if [ -n "$2" ]
    then
        message=$2
        
        if [ -n "$3" ]
        then
            env=$3
        else
            env=release
        fi
        
        if [ "$env" == "alpha" ]
        then
            dingtalkAccessToken="$DINGTALK_ACCESS_TOKEN_ALPHA"
        else
            dingtalkAccessToken="$DINGTALK_ACCESS_TOKEN"
        fi
        
        if [ -n "$dingtalkAccessToken" ]
        then
            echo "发送钉钉自定义机器人通知"
            . $scriptsFolder/send-dingtalk-custom-robot-group-message.sh "$dingtalkAccessToken" "$tag" "$message"
        fi
    fi
fi