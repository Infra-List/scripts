#!/usr/bin/env bash

if [ -n "$1" ]
then
    gitTag=$1
    
    gitTagExits=$(git tag -l "$gitTag");
    if [ -n "$gitTagExits" ]; then
        echo "删除已存在的 git tag : $gitTag"
        git tag -d "$gitTag"
        git push origin :refs/tags/"$gitTag"
    fi
    
    echo "设置 git tag 并推送 : $gitTag"
    git tag "$gitTag"
    git push origin "$gitTag"
fi