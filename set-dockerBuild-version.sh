#!/usr/bin/env bash

if [ -n "$1" ]
then
    xmlFile=$1

    if [ -n "$2" ]
    then
        dockerBuildFile=$2
    else
        dockerBuildFile=docker-build.sh
    fi
    
    newVersion=$(awk '/<\/*AssemblyVersion\/*>/{gsub(/[[:space:]]*<\/*AssemblyVersion\/*>/,"");print $0}' $xmlFile)
    echo "修改 $dockerBuildFile : Version=$newVersion"
    sed -i "" "2s/.*/Version=$newVersion/" $dockerBuildFile
fi