#!/usr/bin/env bash

#!/usr/bin/env bash

dockerFolder=/home/docker/docker

if [ -n "$1" ]
then
    projectName=$1
    
    dockerComposeFileName="$dockerFolder/$projectName/docker-compose.yml"
    echo "执行 docker compose -f $dockerComposeFileName up -d"
    docker compose -f "$dockerComposeFileName" up -d
fi