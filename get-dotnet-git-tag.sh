#!/usr/bin/env bash

if [ -n "$1" ]
then
    xmlFile=$1
    
    if [ -n "$2" ]
    then
        env=$2
        
        projectName=$(awk '/<\/*AssemblyTitle\/*>/{gsub(/[[:space:]]*<\/*AssemblyTitle\/*>/,"");print $0}' "$xmlFile")
        newVersion=$(awk '/<\/*AssemblyVersion\/*>/{gsub(/[[:space:]]*<\/*AssemblyVersion\/*>/,"");print $0}' "$xmlFile")
        if [ "$env" == "alpha" ]
        then
            newVersion="$newVersion-alpha"
        fi
        
        gitTag="$projectName/v$newVersion"
#        echo "设置 dotnet 项目的 git tag 并推送 : $gitTag"
#        . $scriptsFolder/set-git-tag.sh "$gitTag"
        echo "$gitTag"
    fi
fi