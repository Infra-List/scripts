#!/usr/bin/env bash

scriptsFolder=scripts

if [ -n "$1" ]
then
    projectFolder=$1
    
    if [ -n "$2" ]
    then
        env=$2
        
        newVersion=$(awk '/<\/*AssemblyVersion\/*>/{gsub(/[[:space:]]*<\/*AssemblyVersion\/*>/,"");print $0}' "$projectFolder/$projectFolder.csproj")
        projectName=$(awk '/<\/*AssemblyTitle\/*>/{gsub(/[[:space:]]*<\/*AssemblyTitle\/*>/,"");print $0}' "$projectFolder/$projectFolder.csproj")
                    
        if [ "$env" == "alpha" ]
        then
            newVersion="$newVersion-alpha"
            defaultMessage="发布成功"
        else
            defaultMessage="发布成功并已上传 Docker 镜像"
        fi
        
        if [ -n "$3" ]
        then
            message=$3
        else
            message="$defaultMessage"
        fi
        
        . $scriptsFolder/ci-send-notification.sh "$projectName:$newVersion" "$message" "$env"
    fi
fi