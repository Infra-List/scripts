#!/usr/bin/env bash

dockerFolder=/home/docker/docker
scriptsFolder=scripts

if [ -n "$1" ]
then
    projectFolder=$1
    
    if [ -n "$2" ]
    then
        projectName=$2
        
        if [ -n "$3" ]
        then
            env=$3
        
            if [ -n "$4" ]
            then
                dockerfileName=$4
            else
                dockerfileName=Dockerfile
            fi
            
            dockerComposeFile="$dockerFolder/$projectName/docker-compose.yml"
            envFile="$dockerFolder/$projectName/.env"
            
            newVersion=$(awk '/<\/*AssemblyVersion\/*>/{gsub(/[[:space:]]*<\/*AssemblyVersion\/*>/,"");print $0}' "$projectFolder/$projectFolder.csproj")
            
            if [ "$env" == "alpha" ]
            then
                newVersion="$newVersion-alpha"
            fi
            
            . $scriptsFolder/ci-docker-deploy.sh "$newVersion" "$dockerComposeFile" "$envFile" "$projectFolder/$dockerfileName" 1
        fi
    fi
fi