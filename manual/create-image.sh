#!/usr/bin/env bash

if [ -n "$1" ]
then
    dockerComposeFileName=$1
else
    dockerComposeFileName=docker-compose.yml
fi

dockerBuildDir=$2

if [ -n "$3" ]
then
    envFile=$3
else
    envFile=.env.production
fi

image=$(awk '/image:/ {print $2}' $dockerComposeFileName)
repository=$(echo "$image" | awk -F ':' '{print $1}')
oldVersion=$(sed '/^Version=/!d;s/.*=//' $envFile)
echo "移除旧镜像：$repository:$oldVersion"
docker rmi "$repository":"$oldVersion"

if [ -n "$dockerBuildDir" ];then
    cd $dockerBuildDir
fi

newVersion=$(sed '/^Version=/!d;s/.*=//' docker-build.sh)
echo "生成新镜像：$repository:$newVersion"
bash docker-build.sh

if [ -n "$dockerBuildDir" ];then
    cd ..
fi

echo "修改 $envFile：Version=$newVersion"
sed -i "1s/.*/Version=$newVersion/" $envFile
