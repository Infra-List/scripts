#!/usr/bin/env bash

dockerComposeFileName=docker-compose.yml
envFile=.env

. ../../scripts/image-push-cli.sh $dockerComposeFileName $envFile