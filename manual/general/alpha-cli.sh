#!/usr/bin/env bash

dockerComposeFileName=docker-compose.yml
dockerBuildDir=publish
envFile=.env

. ../../scripts/alpha-cli.sh $dockerComposeFileName "$dockerBuildDir" $envFile