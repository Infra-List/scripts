#!/usr/bin/env bash

if [ -n "$1" ]
then
    dockerComposeFileName=$1
else
    dockerComposeFileName=docker-compose.yml
fi

dockerBuildDir=$2

if [ -n "$3" ]
then
    envFile=$3
else
    envFile=.env
fi

echo "执行 docker compose down"
docker compose -f $dockerComposeFileName down

image=$(awk '/image:/ {print $2}' $dockerComposeFileName)
repository=$(echo "$image" | awk -F ':' '{print $1}')
oldVersion=$(sed '/^Version=/!d;s/.*=//' $envFile)
echo "移除旧镜像：$repository:$oldVersion"
docker rmi "$repository":"$oldVersion"

if [ -n "$dockerBuildDir" ]
then
    echo "进入 docke build 文件目录：$dockerBuildDir"
    cd $dockerBuildDir
fi

newVersion=$(sed '/^Version=/!d;s/.*=//' docker-build.sh)
echo "生成新镜像：$repository:$newVersion"
bash docker-build.sh

if [ -n "$dockerBuildDir" ]
then
    echo "返回 docke build 文件目录上一层"
    cd ..
fi

echo "修改 $envFile：Version=$newVersion"
sed -i "1s/.*/Version=$newVersion/" $envFile

echo "执行 docker compose up"
docker compose -f $dockerComposeFileName up
