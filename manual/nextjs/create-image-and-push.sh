#!/usr/bin/env bash

dockerComposeFileName=docker-compose.yml
dockerBuildDir=publish-production
envFile=.env.production

echo "生成镜像"
. ../../scripts/create-image.sh $dockerComposeFileName "$dockerBuildDir" $envFile

echo "push 镜像"
. ../../scripts/image-push-cli.sh $dockerComposeFileName $envFile