#!/usr/bin/env bash

dockerFolder=/home/docker/docker
scriptsFolder=scripts

if [ -n "$1" ]
then
    projectName=$1
    
    dockerComposeFileName="$dockerFolder/$projectName/docker-compose.yml"
    envFile="$dockerFolder/$projectName/.env"
    
    . $scriptsFolder/ci-docker-image-push.sh "$dockerComposeFileName" "$envFile"
fi