#!/usr/bin/env bash

if [ -n "$1" ]
then
    projectName=$1
    
    if [ -n "$2" ]
    then
        newVersion=$2
        
        if [ -n "$3" ]
        then
            env=$3
            
            if [ "$env" == "alpha" ]
            then
                serverSSHPrivateKey="$SERVER_SSH_PRIVATE_KEY_TEST"
                serverHost="$SERVER_HOST_TEST"
                serverUserName="$SERVER_USER_NAME_TEST"
            else
                serverSSHPrivateKey="$SERVER_SSH_PRIVATE_KEY"
                serverHost="$SERVER_HOST"
                serverUserName="$SERVER_USER_NAME"
            fi
            
            if [ -n "$serverSSHPrivateKey" ]
            then       
                eval "$(ssh-agent -s)"
                chmod 400 "$serverSSHPrivateKey"
                ssh-add "$serverSSHPrivateKey"
                mkdir -p ~/.ssh
                chmod 700 ~/.ssh
                
                envFile=".env"
                ssh -o StrictHostKeyChecking=no "$serverUserName"@"$serverHost" "cd $SERVER_DOCKER_SCRIPTS_FOLDER && . ci-docker-hub-run.sh $projectName $newVersion $envFile $DOCKER_HUB_USER_NAME $DOCKER_HUB_PASSWORD"
            fi
        fi
    fi
fi